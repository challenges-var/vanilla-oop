class Dice {
  constructor(sides) {
    this.sides = sides;
    this.roll = () => {
      let randomNumber = Math.floor(Math.random() * this.sides) + 1;
      return randomNumber;
    }
  }
};

let dice = new Dice(6);


const printNumber = (number) => {
  let placeholder = document.getElementById('placeholder');
  placeholder.innerHTML = number;
}

const button = document.getElementById('button');

button.onclick = () => {
  let result = dice.roll();
  printNumber(result);
}