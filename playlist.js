// Playlist
class Playlist {
  constructor() {
    this.songs = [];
    this.nowPlayingIndex = 0;
  }
};

Playlist.prototype.add = function(song) {
  this.songs.push(song);
};

Playlist.prototype.play = function() {
  let currentSong = this.songs[this.nowPlayingIndex];
  currentSong.play();
};

Playlist.prototype.stop = function() {
  let currentSong = this.songs[this.nowPlayingIndex];
  currentSong.stop();
};

Playlist.prototype.next = function() {
  this.stop();
  this.nowPlayingIndex++;
  if(this.nowPlayingIndex === this.songs.length) {
    this.nowPlayingIndex = 0;
  }
  this.play();
};

Playlist.prototype.renderInElement = function(list) {
  list.innerHTML = '';
  for (let i = 0; i < this.songs.length; i++) {
    list.innerHTML += this.songs[i].toHTML();
  }
};

// Song & Media

function Song(title, artist, duration) {
  Media.call(this, title, duration);
  this.artist = artist;
}

function Movie(title, year, duration) {
  Media.call(this, title, duration);
  this.year = year;
}

function Media(title, duration) {
  this.title = title;
  this.duration = duration;
  this.isPlaying = false;
}

Song.prototype = Object.create(Media.prototype);
Movie.prototype = Object.create(Media.prototype);

Media.prototype.play = function() {
  this.isPlaying = true;
};

Media.prototype.stop = function() {
  this.isPlaying = false;
};

Song.prototype.toHTML = function() {
  let htmlString = '<li';
  if(this.isPlaying) {
    htmlString += ' class="current"';
  }
  htmlString += '>';
  htmlString += `${this.title} - `;
  htmlString += this.artist;
  htmlString += '<span class="duration"> ';
  htmlString += `${this.duration}</span></li>`;

  return htmlString;
};

Movie.prototype.toHTML = function() {
  let htmlString = '<li';
  if(this.isPlaying) {
    htmlString += ' class="current"';
  }
  htmlString += '>';
  htmlString += `${this.title} - `;
  htmlString += this.year;
  htmlString += '<span class="duration"> ';
  htmlString += `${this.duration}</span></li>`;

  return htmlString;
};

// App

let playlist = new Playlist();

const hereComesTheSun = new Song('Here Come The Sun', 'The Beatles', '2:54');
const walkingOnSunshine = new Song('Walking On Sunshine', 'Katrina An The Waves', '3:43');
const manOfSteel = new Movie('Man Of Steel', '2013', '2:23:43');

playlist.add(hereComesTheSun);
playlist.add(walkingOnSunshine);
playlist.add(manOfSteel);

const playlistElements = document.getElementById('playlist');

playlist.renderInElement(playlistElements);

const playBtn = document.getElementById('play');
playBtn.onclick = function() {
  playlist.play();
  playlist.renderInElement(playlistElements);
}
const nextBtn = document.getElementById('next');
nextBtn.onclick = function() {
  playlist.next();
  playlist.renderInElement(playlistElements);
}
const stopBtn = document.getElementById('stop');
stopBtn.onclick = function() {
  playlist.stop();
  playlist.renderInElement(playlistElements);
}

